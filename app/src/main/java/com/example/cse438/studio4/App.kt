package com.example.cse438.studio4

import android.app.ActionBar
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.example.cse438.studio4.model.Review
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class App {
    companion object {
        var firebaseAuth: FirebaseAuth? = null

        fun openReviewDialog(context: Context, itemId: String) {
            val dialog = Dialog(context)

            dialog.setContentView(R.layout.dialog_add_review)

            val window = dialog.window
            window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

            dialog.findViewById<Button>(R.id.close).setOnClickListener {
                dialog.dismiss()
            }

            dialog.findViewById<Button>(R.id.submit).setOnClickListener {
                // TODO: Finish implementing using given pseudocode
                //xjq begin
                var body = R.id.body
                var isAnonymous = R.id.anonymous
                var localuserId = firebaseAuth?.currentUser!!.uid

                if(firebaseAuth?.getCurrentUser() != null && body.toString().length != 0){
                    val db = FirebaseFirestore.getInstance()
                    //firebase call, first collection, then document
                    db.collection("items").document("itemId").get().addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            var tempreview = task.result?.get("reviews") as? ArrayList<Review>

                            if(tempreview == null){
                                tempreview = ArrayList<Review>()
                            }

                            db.collection("users").document(localuserId).get().addOnCompleteListener{task2 ->
                                if(task2.isSuccessful){
                                    var userdata = task2.result

                                    var userreview = userdata?.get("reviews") as? ArrayList<String>
                                    //var userreview = userdata?.get("reviews")
                                    if(userreview == null){
                                        userreview = ArrayList<String>()
                                    }
                                    if(!userreview.contains(itemId)){
                                        userreview.add(itemId)
                                    }
                                    //Add a new Review object to the reviews Arraylist
                                    tempreview.add(
                                        Review(
                                            body as String,
                                            isAnonymous as Boolean,
                                            localuserId,
                                            userdata!!.get("username") as String,
                                            Timestamp.now() as Timestamp
                                        )
                                    )
                                    //create a HashMap
                                    val reviewinfo = HashMap<String, Any>()
                                    reviewinfo["reviews"] = tempreview.toList()
                                    //
                                    db.collection("items").document("itemId").set(reviewinfo)

                                    //create another hashmap
                                    val userviewinfo = HashMap<String, Any?>()
                                    userviewinfo["first_name"] = userdata?.get("first_name")
                                    userviewinfo["last_name"] = userdata?.get("last_name")
                                    userviewinfo["email"] = userdata?.get("email")
                                    userviewinfo["username"] = userdata?.get("username")
                                    //include reviews made by the user
                                    userviewinfo["reviews"] = tempreview.toList()
                                    db.collection("users").document(localuserId).set(userviewinfo)

                                    Toast.makeText(context," Review Submitted!",Toast.LENGTH_SHORT).show()
                                    dialog.dismiss()
                                }
                            }
                        }
                    }
                }
                else{
                    Toast.makeText(context," Unable to submit review!",Toast.LENGTH_SHORT).show()
                }
                //xjq end
            }
            dialog.show()
        }
    }
}